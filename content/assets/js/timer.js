// https://www.w3schools.com/howto/howto_js_countdown.asp
// Thanks to W3Schools!

document.addEventListener(
	"DOMContentLoaded",
	function() {
		// Set the date we're counting down to
		var countDownDate = new Date("Sep 9, 2022 12:00:00").getTime();
		
		function timer() {
			
			// Get today's date and time
			var now = new Date().getTime();
			
			// Find the distance between now and the count down date
			var distance = countDownDate - now;
			
			// Time calculations for days, hours, minutes and seconds
			var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);
			
			// Display the result in the element with id="timer"
			const element = document.getElementById("timer");
			
			element.innerHTML = (
				days + " dní, "
				+ hours + " hodin, "
				+ minutes + " minut a "
				+ seconds + " sekund"
			);
			
			// If the count down is finished, write some text
			if (distance < 0) {
				clearInterval(timer);
				element.innerHTML = "Dnes to bude!";
			}
		}
		
		timer();
		
		// Update the count down every 1 second
		const timerInterval = setInterval(timer, 1000);
	}
);
